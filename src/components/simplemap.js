import React, { Component } from 'react'
import { Map, Marker, Popup, TileLayer } from 'react-leaflet'

export default class SimpleMap extends React.Component {

  constructor() {
		super();
		this.state = { 
      hasLocation: false,
      latlng: {
        lat: 61.4978,
        lng: 23.7610,
      },
      zoom: 10,
      markers: []
    };
  }

  
  addMarker = (e) => {
    console.log(e.latlng)
    let _markers = this.state.markers;
    _markers.push(e.latlng)
    this.setState({
      locations: _markers
    })
  }


  handleLocationFound = e => {
    this.setState({
      hasLocation: true,
      latlng: e.latlng,
    })
  }


  render() {
    const position = [this.state.lat, this.state.lng]

    const marker = this.state.hasLocation ? (
      <Marker position={this.state.latlng}>
        <Popup>
          <span>You are here</span>
        </Popup>
      </Marker>
    ) : null

    return (
      <Map center={this.state.latlng}
        length={4}
        onClick={this.addMarker}
        onLocationfound={this.handleLocationFound}
        ref="map"
        zoom={this.state.zoom}>
        <TileLayer
          attribution="&amp;copy <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        {this.state.markers.map((position, idx) => 
          <Marker key={`marker-${idx}`} position={position}>
          <Popup>
            <span>A pretty CSS3 popup. <br/> Easily customizable.</span>
          </Popup>
        </Marker>
        )}
      </Map>
    )
  }
}