import React, { Component } from 'react';
import './App.css';

import SimpleMap from "./components/simplemap";


class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Hellcome to React</h1>
          <h3>with Leaflet maps</h3>
        </header>
        <SimpleMap />
      </div>
    );
  }
}

export default App;
